# Firefox Pastel Dark Theme

A theme for firefox, which consists of 2 parts:
  - Just theme, which you can find on the [firefox extensions site](https://addons.mozilla.org/en-US/firefox/addon/pastel-dark/)
  - Custom css for firefox(userchrome)

## Contents:
  - [Images](#images)
  - [Installation](#installation)
  - [Issues](#issues)

## Images:

<img src="/Images/theme.png" alt="Image of just theme" width="80%"/>

## Installation
Go [here](https://addons.mozilla.org/en-US/firefox/addon/pastel-dark/) and press "Install Theme"
